import styles from './index.less';
import { useState } from 'react';

export default function IndexPage() {
  const [show, setShow] = useState(false);
  const [font, setFont] = useState('');

  const showModel = () => {
    setFont('');
    setShow(true);
  };

  const isOk = () => {
    if (confirm('你真的要确认吗？')) {
      setFont('点击确认了');
      setShow(false);
    }
    return;
  };

  return (
    <div>
      <button onClick={showModel}>打开对话框</button>
      <h2>{font}</h2>
      {show && (
        <div className={styles.modelShow}>
          <div className={styles.modelMain}>
            <div className={styles.modelFont}>23333</div>
            <div className={styles.modelFoot}>
              <button>关闭</button>
              <button onClick={isOk}>确认</button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
