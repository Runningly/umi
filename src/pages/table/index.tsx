import React, { Component } from 'react'
import { Table } from 'antd'

export default () => {

  const dataSource = [
    {
      key: '1',
      sum: '总数',
      p1: 22,
      p2: 32,
      p3: '西湖区湖底公园1号',
    },
  ];
  
  const columns = [
    {
      title: '',
      dataIndex: 'sum',
      key: 'sum',
    },
    {
      title: 'p1',
      dataIndex: 'p1',
      key: 'p1',
    },
    {
      title: 'p2',
      dataIndex: 'p2',
      key: 'age',
    },
    {
      title: 'p3',
      dataIndex: 'p3',
      key: 'p3',
    },
    // ...
  ];
  
  return (
      <Table dataSource={dataSource} columns={columns} />
  )
}