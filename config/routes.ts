export default [
  {
    path: '/',
    redirect: '/index',
  },
  {
    path: '/index',
    name: '首页',
    icon: 'HomeOutlined',
    component: 'home/index',
    access: 'readHome',
  },
  {
    path: '/demo',
    name: '测试页面',
    icon: 'dashboard',
    routes: [
      {
        path: '/demo/module',
        name: '简易模态框',
        component: 'demo/index',
        access: 'readModule',
      },
      {
        path: '/demo/table',
        name: '表格',
        component: 'table/index',
      },
      {
        path: '/demo/workplace',
        name: 'workplace',
      },
    ],
  },
];
