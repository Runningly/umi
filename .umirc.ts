import { defineConfig } from 'umi';
import routes from './config/routes'

export default defineConfig({
  layout:{
    name: 'liyou',
    logo: 'https://preview.pro.ant.design/static/logo.f0355d39.svg',
    // copy from pro site
    navTheme: 'dark',
    primaryColor: '#1890ff',
    layout: 'sidemenu',
    // contentWidth: 'Fluid',
    // fixedHeader: false,
    // fixSiderbar: false,
    title: 'liyou',
    pwa: false,
    iconfontUrl: '',
  },
  nodeModulesTransform: {
    type: 'none',
  },
  routes,
  fastRefresh: {},
});
